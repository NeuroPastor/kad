<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model {

    protected $fillable = ["id","name","english_name","alphabetic_code","digit_code","rate"];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

}
