<?php

namespace App\Console\Commands;


use Exception;
use Illuminate\Console\Command;

use App\Currency;



class CurrencyCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "currency:ref";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Refresh currencies rate from CBR";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $nowDate = date("d/m/Y");

            $xmldata = file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp?date_req=".$nowDate);
            $names = file_get_contents("http://www.cbr.ru/scripts/XML_val.asp?d=0");
            $xml = new \SimpleXMLElement($xmldata);
            $namesXml = new \SimpleXMLElement($names);
            foreach($xml->Valute as $v){
                $data = $namesXml->xpath("//Item[@ID='".$v->attributes()->ID."']");
                $cur = Currency::updateOrCreate(
                    ['id' =>  $v->attributes()->ID],
                    ["name" => $data[0]->Name,
                    "english_name" => $data[0]->EngName,
                    "alphabetic_code" => $v->CharCode,
                    "digit_code" => $v->NumCode,
                    "rate" => $v->Value/$v->Nominal
                    ]
                );
            }

    }
}