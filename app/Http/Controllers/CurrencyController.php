<?php 
namespace App\Http\Controllers;

use \App\Currency;

class CurrencyController extends Controller {

    const MODEL = "App\Currency";

    //use RESTActions;

    public function getAll(){
        $cur = Currency::get();
        return response()->json($cur);
    }
    public function getOne($id){
        $cur = Currency::where('id','=',$id)->get();
        return response()->json($cur);
        
    }

}
