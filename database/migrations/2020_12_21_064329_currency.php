<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Currency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('currencies', function(Blueprint $t){
            $t->string('id')->comment('идентификатор');
            $t->string('name')->comment('название валюты на русском языке');
            $t->string('english_name')->comment('название валюты на английском языке');
            $t->string('alphabetic_code')->comment(' буквенный код валюты в ISO 4217');
            $t->string('digit_code')->comment('цифровой код валюты в ISO 4217');
            $t->float('rate')->comment('курс валюты к рублю, значение должно позволять выполнять конвертацию валют'); //Изменить на float
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
